# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://dariant1@bitbucket.org/dariant1/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/dariant1/stroboskop/commits/0a50efe676ce79e199b818bbfa51e67383d32ac1

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/dariant1/stroboskop/commits/8bb66ae7cc2598094b2e32d3369678fa739b4018

Naloga 6.3.2:
https://bitbucket.org/dariant1/stroboskop/commits/f6a961123b5633264baf2ba7f141a5b20132a7a4

Naloga 6.3.3:
https://bitbucket.org/dariant1/stroboskop/commits/dd1a7f095f1c3651a43a19972b7e197c20a77a44

Naloga 6.3.4:
https://bitbucket.org/dariant1/stroboskop/commits/c958f267036f85084f203de4a52fdeaf58bc0bfd

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/dariant1/stroboskop/commits/cdbd7d4145ddcdca27abcff251840fa33fa25bc3

Naloga 6.4.2:
https://bitbucket.org/dariant1/stroboskop/commits/119699bfd42a364be493d46f3c8d5df61e1d3b19

Naloga 6.4.3:
https://bitbucket.org/dariant1/stroboskop/commits/0c0a357fe5bbf9b915c52e831be2fb6d6b89e8c6

Naloga 6.4.4:
https://bitbucket.org/dariant1/stroboskop/commits/a291ddaf9837a55bb641bef90086500815388f86